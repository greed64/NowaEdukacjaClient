﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NowaEdukacjaClient.Startup))]
namespace NowaEdukacjaClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
