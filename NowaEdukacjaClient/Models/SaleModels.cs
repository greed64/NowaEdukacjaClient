﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NowaEdukacjaClient.Models
{
    #region Modele proste
    public class SalaModel
    {
        public int Id { get; set; }
        public string NumerSali { get; set; }
        public string Budynek { get; set; }
        public string TypSali { get; set; }
        public int LiczbaMiejsc { get; set; }
        public string Wyposazenie { get; set; }
    }

    public class WyposazenieListModel
    {
        public int Id { get; set; }
        public string Typ { get; set; }
    }

    public class TypSaliListModel
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
    }
    #endregion Modele proste

    #region ViewModele
    public class SaleViewModel
    {
        //pola wyszukiwania
        public string NumerSali { get; set; }
        public string Budynek { get; set; }
        public int? TypSali { get; set; }
        public List<int> Wyposazenie { get; set; } = new List<int>();
        public int? LimitMiejscMin { get; set; }
        public int? LimitMiejscMax { get; set; }

        //nieedytowalne
        public List<SalaModel> Sale { get; set; } = new List<SalaModel>();
    }

    public class SaleSearchModel
    {
        public string NumerSali { get; set; }
        public string Budynek { get; set; }
        public int? TypSali { get; set; }
        public List<int> Wyposazenie { get; set; } = new List<int>();
        public int? LimitMiejscMin { get; set; }
        public int? LimitMiejscMax { get; set; }
    }

    public class SalaAddModel
    {
        public string NumerSali { get; set; }
        public string Budynek { get; set; }
        public int TypSali { get; set; }
        public List<int> Wyposazenie { get; set; } = new List<int>();
        public int LiczbaMiejsc { get; set; }
    }
    #endregion ViewModele
}