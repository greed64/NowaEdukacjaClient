﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NowaEdukacjaClient.Models
{
    public class JednostkaOrganizacyjnaListModel
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
    }

    public class GenericListModel
    {
        public int Id { get; set; }
        public string Nazwa { get; set; }
    }

    public class PlanStudiowListModel
    {
        public int Id { get; set; }
        public string Stopien { get; set; }
        public string Tryb { get; set; }
        public string Jezyk { get; set; }
    }

    public class KursPelnyListModel
    {
        public int Id { get; set; }
        public string FormaZajec { get; set; }
        public int LiczbaGodzin { get; set; }
    }

    public class PracownikModel
    {
        public int Id { get; set; }
        public string JednostkaOrganizacyjna { get; set; }
        public string NazwiskoImie { get; set; }
        public string TytulNaukowy { get; set; }
        public string Stanowisko { get; set; }
        public int Pensum { get; set; }
        public string Zainteresowania { get; set; }
    }

    public class PowierzenieModel
    {
        public int Id { get; set; }
        public string Kurs { get; set; }
        public string FormaZajec { get; set; }
        public int LiczbaGodzin { get; set; }
        public int LiczbaGrupZaakceptowana { get; set; }
        public int LiczbaGrupPowierzona { get; set; }
        public string Pracownicy { get; set; }
        public bool CzyPowierzono { get; set; }
        public string Komentarz { get; set; }

        public string JednOrg { get; set; }
    }

    public class PowierzenieAddModel
    {
        public int IdKurs { get; set; } = -1;
        public int LiczbaGrup { get; set; }
        public int IdPracownik { get; set; } = -1;
    }

    public class PrzydzielajSearchModel
    {
        //pracownik
        public List<int> JednostkiOrganizacyjne { get; set; } = new List<int>();
        public string Nazwisko { get; set; }
        public string Imie { get; set; }
        public string Zainteresowanie { get; set; }
        public string Tytul { get; set; }

        //powierzenie
        public int CyklKsztalcenia { get; set; } = -1;
        public int PlanStudiow { get; set; } = -1;
        public int Semestr { get; set; } = -1;
    }

    public class PrzydzielajViewModel
    {
        public List<int> JednostkiOrganizacyjne { get; set; } = new List<int>();
        public string Nazwisko { get; set; }
        public string Imie { get; set; }
        public string Zainteresowanie { get; set; }
        public string Tytul { get; set; }

        public int CyklKsztalcenia { get; set; }
        public int PlanStudiow { get; set; }
        public int Semestr { get; set; }

        public List<PracownikModel> Pracownicy { get; set; } = new List<PracownikModel>();
        public List<PowierzenieModel> Powierzenia { get; set; } = new List<PowierzenieModel>();
    }

    public class MojeViewModel
    {
        public int IdPracownik { get; set; }

        public int CalkowitePensum { get; set; }
        public int WykorzystanePensum { get; set; }

        public List<PowierzenieModel> Zaakceptowane { get; set; } = new List<PowierzenieModel>();
        public List<PowierzenieModel> Propozycje { get; set; } = new List<PowierzenieModel>();
    }

    public class MojeAddModel
    {
        public bool Accepted { get; set; } = false;

        public int Id { get; set; }
        public bool Zaakceptowane { get; set; }
        public int LiczbaGrupPowierzona { get; set; }
        public int LiczbaGrupZaakceptowana { get; set; }
        public string Komentarz { get; set; }
    }
}