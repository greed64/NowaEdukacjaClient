﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NowaEdukacjaClient.Models
{
    public class ResponseDialog
    {
        public bool Success { get; set; }
        public string Body { get; set; }
    }
}