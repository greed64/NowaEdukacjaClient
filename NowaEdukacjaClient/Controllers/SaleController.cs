﻿using NowaEdukacjaClient.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NowaEdukacjaClient.Controllers
{
    public class SaleController : BaseNowaEdukacjaController
    {
        PlanowanieSalService.PlanowanieSalService serwisSale = new PlanowanieSalService.PlanowanieSalService();

        // GET: Szukaj
        public ActionResult Szukaj(SaleSearchModel search)
        {
            //wskazanie wejściowego modelu
            SaleSearchModel searchModel;

            SaleSearchModel tempSearchModel = (SaleSearchModel)TempData["search"];
            if (tempSearchModel != null)
            {
                searchModel = tempSearchModel;
            }
            else
            {
                searchModel = search;
            }

            {
                Debug.WriteLine($"searchModel.NumerSali {searchModel.NumerSali}");
                Debug.WriteLine($"searchModel.Budynek {searchModel.Budynek}");
                Debug.WriteLine($"searchModel.TypSali {searchModel.TypSali}");
                Debug.WriteLine($"searchModel.Wyposazenie {searchModel.Wyposazenie}");
                Debug.WriteLine($"searchModel.LimitMiejscMin {searchModel.LimitMiejscMin}");
                Debug.WriteLine($"searchModel.LimitMiejscMax {searchModel.LimitMiejscMax}");
            }//HACK usuń!

            //utworzenie pustego ViewModela
            SaleViewModel viewModel = new SaleViewModel();

            try
            {
                //odczyt z serwisów, wstępna filtracja
                List<PlanowanieSalService.Sala> przefiltrowaneSale;

                PlanowanieSalService.Budynek[] wszystkieBudynki = serwisSale.getAllBuildings();
                PlanowanieSalService.WyposazenieSali[] wszystkieWyposazenie = serwisSale.getAllEquipment();
                PlanowanieSalService.TypSali[] wszystkieTypy = serwisSale.getAllClassroomTypes();
                PlanowanieSalService.Sala[] wszystkieSale = serwisSale.getAllClassrooms();

                IEnumerable<PlanowanieSalService.Sala> filtrowaneSale = wszystkieSale.AsEnumerable();//TODO jeśli zdążymy: filtrowanie powinno być po stronie serwisu

                if (!string.IsNullOrWhiteSpace(searchModel.NumerSali))
                {
                    string num = searchModel?.NumerSali;
                    filtrowaneSale = filtrowaneSale.Where(s => s.Numer == num);
                }
                if (searchModel.Budynek != null)
                {
                    string bud = searchModel?.Budynek;
                    int? idBud = wszystkieBudynki.Where(b => b.Nazwa == bud).FirstOrDefault()?.Id;
                    if (idBud != null)
                    {
                        filtrowaneSale = filtrowaneSale.Where(s => s.IdBudynek == idBud);
                    }
                    else
                    {
                        filtrowaneSale = filtrowaneSale.Take(0);
                    }
                }
                if (searchModel.TypSali != null)
                {
                    int typ = searchModel.TypSali.Value;
                    filtrowaneSale = filtrowaneSale.Where(s => (int)(s.Typ) == typ);
                }
                if (searchModel.LimitMiejscMin != null)
                {
                    int limMin = searchModel.LimitMiejscMin.Value;
                    filtrowaneSale = filtrowaneSale.Where(s => s.LiczbaMiejsc >= limMin);
                }
                if (searchModel.LimitMiejscMax != null)
                {
                    int limMax = searchModel.LimitMiejscMax.Value;
                    filtrowaneSale = filtrowaneSale.Where(s => s.LiczbaMiejsc <= limMax);
                }

                Dictionary<int, PlanowanieSalService.WyposazenieSali[]> wyposazenieWSalach = new Dictionary<int, PlanowanieSalService.WyposazenieSali[]>();
                foreach (var sala in filtrowaneSale)
                {
                    PlanowanieSalService.WyposazenieSali[] wyp = serwisSale.getEquipmentByClassroom(sala.Id, true);//TODO isSpecified jest niepotrzebny?
                    wyposazenieWSalach.Add(sala.Id, wyp);
                }

                if (searchModel.Wyposazenie.Count > 0)
                {
                    przefiltrowaneSale = new List<PlanowanieSalService.Sala>();

                    foreach (var sala in filtrowaneSale)
                    {
                        PlanowanieSalService.WyposazenieSali[] wyp = wyposazenieWSalach[sala.Id];
                        if (searchModel.Wyposazenie.Intersect(wyp.Select(w => w.Id)).Count()
                            == searchModel.Wyposazenie.Count)
                        {
                            przefiltrowaneSale.Add(sala);
                        }
                    }
                }
                else
                {
                    przefiltrowaneSale = filtrowaneSale.ToList();
                }

                //zapełnienie ViewModela
                viewModel.NumerSali = searchModel.NumerSali;
                viewModel.Budynek = searchModel.Budynek;
                viewModel.TypSali = searchModel.TypSali;
                if (searchModel.Wyposazenie != null)
                {
                    viewModel.Wyposazenie = searchModel.Wyposazenie;
                }
                viewModel.LimitMiejscMin = searchModel.LimitMiejscMin;
                viewModel.LimitMiejscMax = searchModel.LimitMiejscMax;

                foreach (var sala in przefiltrowaneSale)
                {
                    int idBudynek = sala.IdBudynek;
                    StringBuilder sb = new StringBuilder();
                    PlanowanieSalService.WyposazenieSali[] wypo = wyposazenieWSalach[sala.Id];

                    if (wypo.Length > 0)
                    {
                        sb.Append(wypo[0].Nazwa);
                        for (int i = 1; i < wypo.Length; i++)
                        {
                            sb.Append(", ");
                            sb.Append(wypo[i].Nazwa);
                        }
                    }

                    viewModel.Sale.Add(new SalaModel
                    {
                        Id = sala.Id,
                        NumerSali = sala.Numer,
                        Budynek = wszystkieBudynki.Where(b => b.Id == idBudynek).FirstOrDefault()?.Nazwa,
                        TypSali = Helpers.nazwaTypSali(sala.Typ),
                        LiczbaMiejsc = sala.LiczbaMiejsc,
                        Wyposazenie = sb.ToString()
                    });
                }

                List<TypSaliListModel> typySal = new List<TypSaliListModel>();
                foreach (var ts in wszystkieTypy)
                {
                    typySal.Add(new TypSaliListModel { Id = (int)ts, Nazwa = Helpers.nazwaTypSali(ts) });
                }

                List<WyposazenieListModel> typyWyposazenia = new List<WyposazenieListModel>();
                foreach (var w in wszystkieWyposazenie)
                {
                    typyWyposazenia.Add(new WyposazenieListModel { Id = w.Id, Typ = w.Nazwa });
                }

                ViewBag.typySal = typySal;
                ViewBag.typyWyposazenia = typyWyposazenia;
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");
                
                //ViewBagi zawierające list/arraye/cokolwiek co można dać do foreach() nie mogą być nullem
                ViewBag.typySal = new List<TypSaliListModel>();
                ViewBag.typyWyposazenia = new List<WyposazenieListModel>();
            }

            return View(viewModel);
        }

        // POST Dodaj
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Dodaj(SalaAddModel Dodaj, SaleSearchModel Szukaj)
        {
            {
                Debug.WriteLine($" # Dodaj.NumerSali: {Dodaj.NumerSali}");
                Debug.WriteLine($" # Dodaj.Budynek: {Dodaj.Budynek}");
                Debug.WriteLine($" # Dodaj.TypSali: {Dodaj.TypSali}");
                Debug.WriteLine($" # Dodaj.Wyposazenie: {Dodaj.Wyposazenie}");
                Debug.WriteLine($" # Dodaj.LiczbaMiejsc: {Dodaj.LiczbaMiejsc}");
            }//HACK usuń

            try
            {
                bool sukcesSQL;

                if (Dodaj.TypSali == -1)
                {
                    RespondError("Typ sali musi być wybrany!");

                    TempData["search"] = Szukaj;

                    return RedirectToAction("Szukaj");
                }

                int idBudynku;

                PlanowanieSalService.Budynek budynek = serwisSale.getAllBuildings()
                    .Where(b => b.Nazwa == Dodaj.Budynek)
                    .FirstOrDefault();

                if (budynek == null)
                {
                    serwisSale.addBuilding(new PlanowanieSalService.Budynek
                    {
                        Nazwa = Dodaj.Budynek
                    }, out idBudynku, out sukcesSQL);

                    if (!sukcesSQL)
                    {
                        RespondError("Nie udało się dodać nowego budynku!<br />Nie dodano sali.");

                        TempData["search"] = Szukaj;

                        return RedirectToAction("Szukaj");
                    }
                }
                else
                {
                    idBudynku = budynek.Id;
                }

                Debug.WriteLine($"Budynek: {Dodaj.Budynek}; Id budynku: {idBudynku}");

                int resultId;

                serwisSale.addClassroom(new PlanowanieSalService.Sala
                {
                    Numer = Dodaj.NumerSali,
                    IdBudynek = idBudynku,
                    IdBudynekSpecified = true,
                    Typ = (PlanowanieSalService.TypSali)Dodaj.TypSali,
                    TypSpecified = true,
                    LiczbaMiejsc = Dodaj.LiczbaMiejsc,
                    LiczbaMiejscSpecified = true

                }, out resultId, out sukcesSQL);

                if (!sukcesSQL)
                {
                    RespondError("Wystąpił błąd serwisu!<br />Nie dodano.");

                    TempData["search"] = Szukaj;

                    return RedirectToAction("Szukaj");
                }

                PlanowanieSalService.WyposazenieSali[] wszystkieWyposazenie = serwisSale.getAllEquipment();

                foreach (int wid in Dodaj.Wyposazenie)
                {
                    int temp;
                    serwisSale.addEquipment(resultId, true,
                        wszystkieWyposazenie.Where(w => w.Id == wid).First(),
                        out temp, out sukcesSQL);

                    if (!sukcesSQL)
                    {
                        RespondError("Nie udało się dodać wyposażenia!<br />Nie dodano sali.");

                        TempData["search"] = Szukaj;

                        return RedirectToAction("Szukaj");
                    }
                }

                RespondSuccess($"Dodano salę {Dodaj.NumerSali} w budynku {Dodaj.Budynek}.");
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");
            }

            TempData["search"] = Szukaj;

            return RedirectToAction("Szukaj");
        }

        // POST Usun
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Usun(int id, SaleSearchModel Szukaj)
        {
            {
                Debug.WriteLine($" # Szukaj.NumerSali: {Szukaj.NumerSali}");
                Debug.WriteLine($" # Szukaj.Budynek: {Szukaj.Budynek}");
                Debug.WriteLine($" # Szukaj.TypSali: {Szukaj.TypSali}");
                Debug.WriteLine($" # Szukaj.Wyposazenie: {Szukaj.Wyposazenie}");
                Debug.WriteLine($" # Szukaj.LimitMiejscMin: {Szukaj.LimitMiejscMin}");
                Debug.WriteLine($" # Szukaj.LimitMiejscMax: {Szukaj.LimitMiejscMax}");
            }//HACK usuń

            Debug.WriteLine($" # id: {id}");

            try
            {
                serwisSale.removeClassroom(id, true);
                RespondSuccess($"Usunięto salę.");
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");
            }

            TempData["search"] = Szukaj;

            return RedirectToAction("Szukaj");
        }
    }
}