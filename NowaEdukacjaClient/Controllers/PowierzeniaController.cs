﻿using NowaEdukacjaClient.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NowaEdukacjaClient.Controllers
{
    public class PowierzeniaController : BaseNowaEdukacjaController
    {
        PlanowaniePowierzenService.PlanowaniePowierzenService serwisPowierzenia = new PlanowaniePowierzenService.PlanowaniePowierzenService();
        ZIRPService.ZatwierdzanieIRaportowaniePowierzenService serwisZirp = new ZIRPService.ZatwierdzanieIRaportowaniePowierzenService();

        // GET: Przydzielaj
        public ActionResult Przydzielaj(PrzydzielajSearchModel Szukaj)
        {
            PrzydzielajSearchModel search;

            if (TempData["PowierzeniaPrzydzielajSzukaj"] != null)
            {
                search = (PrzydzielajSearchModel)TempData["PowierzeniaPrzydzielajSzukaj"];
            }
            else
            {
                search = Szukaj;
            }

            PrzydzielajViewModel viewModel = new PrzydzielajViewModel();

            viewModel.JednostkiOrganizacyjne = search.JednostkiOrganizacyjne;
            viewModel.Nazwisko = search.Nazwisko;
            viewModel.Imie = search.Imie;
            viewModel.Zainteresowanie = search.Zainteresowanie;
            viewModel.Tytul = search.Tytul;

            viewModel.CyklKsztalcenia = search.CyklKsztalcenia;
            viewModel.PlanStudiow = search.PlanStudiow;
            viewModel.Semestr = search.Semestr;

            try
            {
                //pobranie podstawowych danych z serwisów
                ZIRPService.JednostkaOrganizacyjna[] wszystkieJO = serwisZirp.getAllOrgUnits();//jest już posortowane
                ZIRPService.Stanowisko[] wszystkieStanowiska = serwisZirp.getAllJobTitles();

                ZIRPService.PracownikDydaktyczny[] pracownicyTabela = serwisZirp.getEmployeesByNameInterestTitle(
                    search.Imie,
                    search.Nazwisko,
                    search.Zainteresowanie,
                    search.Tytul);

                ZIRPService.CyklKsztalcenia[] wszystkieCK = serwisZirp.getAllEducationCycles();
                ZIRPService.PlanStudiow[] wszystkiePS = serwisZirp.getAllStudyPlans();
                ZIRPService.Semestr[] wszystkieSemestry = serwisZirp.getAllSemesters();

                ZIRPService.FormaZajec[] wszystkieFormyZajec = serwisZirp.getAllCourseForms();

                ZIRPService.Kurs[] kursy = serwisZirp.getCoursesByCyclePlanSemester(
                    search.CyklKsztalcenia, true,
                    search.PlanStudiow, true,
                    search.Semestr, true);

                int[] idKursow = kursy.Select(k => k.Id).ToArray();

                ZIRPService.Powierzenie[] powierzeniaTabela = serwisZirp.getCommittalsByCourses(idKursow);

                int[] idPowierzen = powierzeniaTabela.Select(p => p.Id).ToArray();

                ZIRPService.PracownikDydaktyczny[] pracownicyOdPowierzen = serwisZirp.getEmployeesByCommittals(idPowierzen);
                ZIRPService.Przedmiot[] przedmiotyOdKursow = serwisZirp.getSubjectsByCourses(idKursow);

                //generacja danych do ViewBagów
                var jednostkiOrg = new List<JednostkaOrganizacyjnaListModel>();
                foreach (var jo in wszystkieJO)
                {
                    jednostkiOrg.Add(new JednostkaOrganizacyjnaListModel
                    {
                        Id = jo.Id,
                        Nazwa = $"({jo.Skrot}) {jo.Nazwa}"
                    });
                }

                var cykleKsztalcenia = new List<GenericListModel>();
                foreach (var ck in wszystkieCK)
                {
                    cykleKsztalcenia.Add(new GenericListModel
                    {
                        Id = ck.Id,
                        Nazwa = $"{ck.Rok1}/{ck.Rok2}"
                    });
                }

                var planyStudiow = new List<GenericListModel>();
                foreach (var ps in wszystkiePS)
                {
                    planyStudiow.Add(new GenericListModel
                    {
                        Id = ps.Id,
                        Nazwa = $"{ps.Kierunek}, {Helpers.nazwaTryb(ps.Tryb)}, st.{ps.Stopien}, język {Helpers.nazwaJezyk(ps.Jezyk)}"
                    });
                }

                var semestry = new List<GenericListModel>();
                foreach (var s in wszystkieSemestry)
                {
                    semestry.Add(new GenericListModel
                    {
                        Id = s.Id,
                        Nazwa = $"{s.Rok} {Helpers.nazwaTypSemestru(s.Typ)}"
                    });
                }

                var kursoPrzedmiot = new List<GenericListModel>();
                foreach (var k in kursy)
                {
                    var przedmiot = przedmiotyOdKursow
                        .Where(p => p.Id == k.IdPrzedmiot)
                        .FirstOrDefault();

                    var forma = wszystkieFormyZajec
                        .Where(fz => fz.Id == k.IdFormaZajec)
                        .FirstOrDefault();

                    kursoPrzedmiot.Add(new GenericListModel
                    {
                        Id = k.Id,
                        Nazwa = $"[{k.Kod}] {przedmiot.nazwa}, {forma.nazwa}"
                    });
                }
                kursoPrzedmiot = kursoPrzedmiot.OrderBy(kp => kp.Nazwa).ToList();

                var formyZajec = new List<GenericListModel>();
                foreach (var fz in wszystkieFormyZajec)
                {
                    formyZajec.Add(new GenericListModel
                    {
                        Id = fz.Id,
                        Nazwa = fz.nazwa
                    });
                }

                var kursyPelne = new List<KursPelnyListModel>();
                foreach (var k in kursy)
                {
                    kursyPelne.Add(new KursPelnyListModel
                    {
                        Id = k.Id,
                        FormaZajec = formyZajec
                            .Where(fz => fz.Id == k.IdFormaZajec)
                            .Select(fz => fz.Nazwa)
                            .FirstOrDefault(),
                        LiczbaGodzin = k.LiczbaGodzinZZU
                    });
                }

                //zapełnianie modelu - tabela pracowników
                bool pusteTez = search.JednostkiOrganizacyjne.Contains(-1);
                foreach (var p in pracownicyTabela)
                {
                    if (search.JednostkiOrganizacyjne.Count == 0
                        || (p.IdJednostkaOrganizacyjna.HasValue
                            && search.JednostkiOrganizacyjne.Contains(p.IdJednostkaOrganizacyjna.Value))
                        || (pusteTez && !p.IdJednostkaOrganizacyjna.HasValue))
                    {
                        StringBuilder sb = new StringBuilder();

                        ZIRPService.ZainteresowaniaAkademickie[] zainteresowania
                            = serwisZirp.getInterestsByEmployee(p.Id, true);//już posortowane

                        if (zainteresowania.Length > 0)
                        {
                            sb.Append(zainteresowania[0].Nazwa);
                            for (int i = 1; i < zainteresowania.Length; i++)
                            {
                                sb.Append(", ");
                                sb.Append(zainteresowania[1].Nazwa);
                            }
                        }

                        ZIRPService.Stanowisko stanowisko = wszystkieStanowiska.Where(s => s.Id == p.IdStanowisko).FirstOrDefault();

                        viewModel.Pracownicy.Add(new PracownikModel
                        {
                            Id = p.Id,
                            JednostkaOrganizacyjna = wszystkieJO.Where(jo => jo.Id == p.IdJednostkaOrganizacyjna).Select(jo => jo.Skrot).FirstOrDefault(),
                            NazwiskoImie = $"{p.Nazwisko}, {p.Imie}",
                            TytulNaukowy = p.TytulyNaukowe,
                            Stanowisko = stanowisko.Nazwa,
                            Pensum = stanowisko.Pensum - p.ObnizeniePensum,
                            Zainteresowania = sb.ToString()
                        });
                    }
                }

                //zapełnianie modelu - tabela powierzeń
                foreach (var pow in powierzeniaTabela)
                {
                    //pracownicyOdPowierzen
                    //przedmiotyOdPowierzen
                    //kursy

                    ZIRPService.Kurs_Powierzenie_PracownikDydaktyczny kppd = serwisZirp.getLinkByCommittal(pow.Id, true);

                    ZIRPService.Kurs kurs = kursy.Where(k => k.Id == kppd.IdKurs).FirstOrDefault();

                    if (kurs == null)
                    {
                        throw new ArgumentNullException($"Mamy do czynienia z powierzeniem-widmo: bez kursu, bez serc, szkieletów to encje.<br/>(id powierzenia: {pow.Id})");
                    }

                    ZIRPService.Przedmiot przedmiot = przedmiotyOdKursow
                        .Where(p => p.Id == kurs.IdPrzedmiot)
                        .FirstOrDefault();

                    if (przedmiot == null)
                    {
                        throw new ArgumentNullException($"Mamy do czynienia z kursem bezprzedmiotowym.<br/>(id powierzenia: {pow.Id})");
                    }

                    ZIRPService.PracownikDydaktyczny pracownik = pracownicyOdPowierzen
                        .Where(p => p.Id == kppd.IdPracownikDydaktyczny)
                        .FirstOrDefault();

                    if (pracownik == null)
                    {
                        throw new ArgumentNullException($"Mamy do czynienia z powierzeniem bezpańskim: ktoś miał być powierzony, a nie jest.<br/>(id powierzenia: {pow.Id})");
                    }

                    viewModel.Powierzenia.Add(new PowierzenieModel
                    {
                        Id = pow.Id,
                        Kurs = $"{przedmiot.nazwa}",
                        FormaZajec = wszystkieFormyZajec.Where(fz => fz.Id == kurs.IdFormaZajec).FirstOrDefault()?.nazwa,
                        LiczbaGodzin = kurs.LiczbaGodzinZZU,
                        LiczbaGrupZaakceptowana = pow.LiczbaGrupZaakceptowana,
                        LiczbaGrupPowierzona = pow.LiczbaGrupProponowana,
                        Pracownicy = $"{pracownik.Nazwisko}, {pracownik.Imie}",
                        CzyPowierzono = pow.Zaakceptowane,
                        Komentarz = pow.Komentarz
                    });
                }

                //zapełnienie ViewBagów
                ViewBag.jednostkiOrg = jednostkiOrg;
                ViewBag.cykleKsztalcenia = cykleKsztalcenia;
                ViewBag.planyStudiow = planyStudiow;
                ViewBag.semestry = semestry;
                ViewBag.kursy = kursoPrzedmiot;
                ViewBag.kursyPelne = kursyPelne;
                ViewBag.formyZajec = formyZajec;
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");

                ViewBag.jednostkiOrg = new List<JednostkaOrganizacyjnaListModel>();
                ViewBag.cykleKsztalcenia = new List<GenericListModel>();
                ViewBag.planyStudiow = new List<GenericListModel>();
                ViewBag.semestry = new List<GenericListModel>();
                ViewBag.kursy = new List<GenericListModel>();
                ViewBag.formyZajec = new List<GenericListModel>();
                ViewBag.kursyPelne = new List<KursPelnyListModel>();
            }

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Przydziel(PowierzenieAddModel Dodaj, PrzydzielajSearchModel Szukaj)
        {
            TempData["PowierzeniaPrzydzielajSzukaj"] = Szukaj;

            try
            {
                int resultId;
                bool successSql;

                serwisZirp.addCommittal(
                    Dodaj.IdKurs, true,
                    Dodaj.IdPracownik, true,
                    Dodaj.LiczbaGrup, true,
                    out resultId, out successSql);

                if (successSql)
                {
                    RespondSuccess($"Dodano nowe powierzenie o id {resultId}.");
                }
                else
                {
                    RespondError("Nie udało się dodać powierzenia, ale nie wiemy dlaczego.");
                }
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");
            }

            return RedirectToAction("Przydzielaj", "Powierzenia");
        }

        public ActionResult Moje(int? PodszyjSie)
        {
            int idPracownik;

            if (TempData["PodszyjSie"] != null)
            {
                idPracownik = (int)TempData["PodszyjSie"];
            }
            else
            {
                idPracownik = PodszyjSie ?? 2;
            }

            MojeViewModel viewModel = new MojeViewModel();
            viewModel.IdPracownik = idPracownik;

            try
            {
                //wywołania serwisu związane z niekompletnością
                ZIRPService.Stanowisko[] wszystkieStanowiska = serwisZirp.getAllJobTitles();
                ZIRPService.PracownikDydaktyczny[] wszyscyPracownicy = serwisZirp.getEmployeesByNameInterestTitle(null, null, null, null);
                ZIRPService.FormaZajec[] wszystkieFormyZajec = serwisZirp.getAllCourseForms();

                //wywołanie właściwych metod serwisu
                //PlanowaniePowierzenService.Pracownik pracownik = serwisPowierzenia.getEmployee(idPracownik, true);//nadmiarowe
                ZIRPService.PracownikDydaktyczny pracownik = wszyscyPracownicy.Where(p => p.Id == idPracownik).FirstOrDefault();
                PlanowaniePowierzenService.Powierzenie[] powierzenia = serwisPowierzenia.getCommitals(idPracownik, true);

                int[] ids_from_committals = powierzenia.Select(p => p.Id).ToArray();
                PlanowaniePowierzenService.Kurs[] kursy = 
                    serwisPowierzenia.getCoursesByCommittals(ids_from_committals);

                int[] ids_from_courses = kursy.Select(k => k.Id).ToArray();
                PlanowaniePowierzenService.Przedmiot[] przedmioty = 
                    serwisPowierzenia.getSubjectsByCourses(ids_from_courses);

                int[] ids_from_subjects = przedmioty.Select(p => p.Id).ToArray();
                PlanowaniePowierzenService.JednostkaOrganizacyjna[] jednostkiOrg =
                    serwisPowierzenia.getOrgUnitsBySubjects(ids_from_subjects);

                //chlew, po prostu chlew
                ZIRPService.Stanowisko stanowisko = wszystkieStanowiska.Where(s => s.Id == pracownik.IdStanowisko).FirstOrDefault();

                int calkowitePensum = stanowisko.Pensum - pracownik.ObnizeniePensum;
                viewModel.CalkowitePensum = calkowitePensum;

                int wykorzystanePensum = 0;

                foreach (var pow in powierzenia)
                {
                    ZIRPService.Kurs_Powierzenie_PracownikDydaktyczny link = serwisZirp.getLinkByCommittal(pow.Id, true);
                    PlanowaniePowierzenService.Kurs kurs = kursy.Where(k => k.Id == link.IdKurs).FirstOrDefault();
                    PlanowaniePowierzenService.Przedmiot przedmiot = przedmioty.Where(p => p.Id == kurs.IdPrzedmiot).FirstOrDefault();
                    PlanowaniePowierzenService.JednostkaOrganizacyjna jo = jednostkiOrg.Where(j => j.Id == przedmiot.IdJednostkaOrganizacyjna).FirstOrDefault();

                    if (pow.Zaakceptowane)
                    {
                        if (pow.LiczbaGrupZaakceptowana > 0)
                        {
                            wykorzystanePensum += kurs.LiczbaGodzinZZU * pow.LiczbaGrupZaakceptowana;

                            viewModel.Zaakceptowane.Add(new PowierzenieModel
                            {
                                Id = pow.Id,
                                Kurs = $"{przedmiot.nazwa}",
                                FormaZajec = wszystkieFormyZajec.Where(fz => fz.Id == kurs.IdFormaZajec).FirstOrDefault()?.nazwa,
                                LiczbaGodzin = kurs.LiczbaGodzinZZU,
                                LiczbaGrupZaakceptowana = pow.LiczbaGrupZaakceptowana,
                                LiczbaGrupPowierzona = pow.LiczbaGrupProponowana,
                                //Pracownicy = $"{pracownik.Nazwisko}, {pracownik.Imie}",
                                CzyPowierzono = pow.Zaakceptowane,
                                Komentarz = pow.Komentarz,
                                JednOrg = jo.Skrot
                            });
                        }
                    }
                    else
                    {
                        viewModel.Propozycje.Add(new PowierzenieModel
                        {
                            Id = pow.Id,
                            Kurs = $"{przedmiot.nazwa}",
                            FormaZajec = wszystkieFormyZajec.Where(fz => fz.Id == kurs.IdFormaZajec).FirstOrDefault()?.nazwa,
                            LiczbaGodzin = kurs.LiczbaGodzinZZU,
                            LiczbaGrupZaakceptowana = pow.LiczbaGrupZaakceptowana,
                            LiczbaGrupPowierzona = pow.LiczbaGrupProponowana,
                            //Pracownicy = $"{pracownik.Nazwisko}, {pracownik.Imie}",
                            CzyPowierzono = pow.Zaakceptowane,
                            Komentarz = pow.Komentarz,
                            JednOrg = jo.Skrot
                        });
                    }
                }

                viewModel.WykorzystanePensum = wykorzystanePensum;

                //zapełnianie ViewBagów
                List<GenericListModel> pracownicy = new List<GenericListModel>();
                foreach (var p in wszyscyPracownicy)
                {
                    pracownicy.Add(new GenericListModel
                    {
                        Id = p.Id,
                        Nazwa = $"{p.TytulyNaukowe} {p.Imie} {p.Nazwisko}"
                    });
                }

                ViewBag.pracownicy = pracownicy;
            }
            catch (Exception e)
            {
                RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");

                ViewBag.pracownicy = new List<GenericListModel>();
            }

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Zmien(int PodszyjSie, List<MojeAddModel> Zmien, int Akcja)
        {
            TempData["PodszyjSie"] = PodszyjSie;
            
            if (Akcja != -1 && Akcja != 0 && Akcja != 1)
            {
                RespondError($"Podano nieprawidłowy numer akcji: {Akcja}.");
            }
            else
            {
                try
                {
                    {
                        Debug.WriteLine($"PodszyjSie: {PodszyjSie}");
                        Debug.WriteLine($"Akcja: {Akcja}");
                        int index = 0;
                        foreach (var z in Zmien)
                        {
                            if (z.Accepted)
                            {
                                Debug.WriteLine($"Zmien[{index}].Accepted: {z.Accepted}");
                                Debug.WriteLine($"Zmien[{index}].Id: {z.Id}");
                                Debug.WriteLine($"Zmien[{index}].LiczbaGrupZaakceptowana: {z.LiczbaGrupZaakceptowana}");
                                Debug.WriteLine($"Zmien[{index}].LiczbaGrupPowierzona: {z.LiczbaGrupPowierzona}");
                                Debug.WriteLine($"Zmien[{index}].Komentarz: {z.Komentarz}");
                                Debug.WriteLine("###");
                            }
                            index++;
                        }
                    }//HACK usuń

                    List<PlanowaniePowierzenService.Powierzenie> doAktualizacji = new List<PlanowaniePowierzenService.Powierzenie>();
                    string nazwaAkcji = "???";

                    if (Akcja == -1)
                    {
                        nazwaAkcji = "odrzucenie";

                        foreach (var z in Zmien)
                        {
                            if (z.Accepted)
                            {
                                doAktualizacji.Add(new PlanowaniePowierzenService.Powierzenie
                                {
                                    ZaakceptowaneSpecified = true,
                                    LiczbaGrupProponowanaSpecified = true,
                                    LiczbaGrupZaakceptowanaSpecified = true,

                                    Id = z.Id,
                                    IdSpecified = true,
                                    LiczbaGrupProponowana = z.LiczbaGrupPowierzona,

                                    Zaakceptowane = true,

                                    LiczbaGrupZaakceptowana = 0,

                                    Komentarz = z.Komentarz
                                });
                            }
                        }
                    }
                    else if (Akcja == 0)
                    {
                        nazwaAkcji = "propozycja zmian dla";

                        foreach (var z in Zmien)
                        {
                            if (z.Accepted)
                            {
                                doAktualizacji.Add(new PlanowaniePowierzenService.Powierzenie
                                {
                                    ZaakceptowaneSpecified = true,
                                    LiczbaGrupProponowanaSpecified = true,
                                    LiczbaGrupZaakceptowanaSpecified = true,

                                    Id = z.Id,
                                    IdSpecified = true,
                                    LiczbaGrupProponowana = z.LiczbaGrupPowierzona,

                                    Zaakceptowane = false,

                                    LiczbaGrupZaakceptowana = z.LiczbaGrupZaakceptowana,

                                    Komentarz = z.Komentarz
                                });
                            }
                        }
                    }
                    else if (Akcja == 1)
                    {
                        nazwaAkcji = "akceptacja";

                        foreach (var z in Zmien)
                        {
                            if (z.Accepted)
                            {
                                doAktualizacji.Add(new PlanowaniePowierzenService.Powierzenie
                                {
                                    ZaakceptowaneSpecified = true,
                                    LiczbaGrupProponowanaSpecified = true,
                                    LiczbaGrupZaakceptowanaSpecified = true,

                                    Id = z.Id,
                                    IdSpecified = true,
                                    LiczbaGrupProponowana = z.LiczbaGrupPowierzona,

                                    Zaakceptowane = true,

                                    LiczbaGrupZaakceptowana = z.LiczbaGrupZaakceptowana,

                                    Komentarz = z.Komentarz
                                });
                            }
                        }
                    }

                    bool successSql;
                    bool resultIGuess;

                    serwisPowierzenia.changeExistingCommittals(
                        doAktualizacji.ToArray(),
                        out successSql,
                        out resultIGuess);

                    if (successSql)
                    {
                        if (resultIGuess)
                        {
                            if (doAktualizacji.Count == 1)
                            {
                                RespondSuccess($"Pomyślnie wykonała się {nazwaAkcji} {doAktualizacji.Count} powierzenia.");
                            }
                            else
                            {
                                RespondSuccess($"Pomyślnie wykonała się {nazwaAkcji} {doAktualizacji.Count} powierzeń.");
                            }
                        }
                        else
                        {
                            RespondError("WebService'owa operacja aktualizacji nie wykonała się z nieznanego powodu.");
                        }
                    }
                    else
                    {
                        RespondError("Komenda SQL aktualizacji nie wykonała się z nieznanego powodu.");
                    }
                }
                catch (Exception e)
                {
                    RespondError($"Wystąpił nieoczekiwany błąd.<br/>{e.Message}<br/>{e.StackTrace}");
                }
            }

            return RedirectToAction("Moje", "Powierzenia");
        }
    }
}