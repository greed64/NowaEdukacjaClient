﻿using NowaEdukacjaClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NowaEdukacjaClient.Controllers
{
    public abstract class BaseNowaEdukacjaController : Controller
    {
        protected void RespondSuccess(string message)
        {
            TempData["ResponseDialog"] = new ResponseDialog
            {
                Success = true,
                Body = message
            };
        }

        protected void RespondError(string message)
        {
            TempData["ResponseDialog"] = new ResponseDialog
            {
                Success = false,
                Body = message
            };
        }
    }
}