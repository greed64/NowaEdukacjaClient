﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NowaEdukacjaClient
{
    public class Helpers
    {
        public static string nazwaTypSali(PlanowanieSalService.TypSali ts)
        {
            string nazwa = "(nieznana)";

            switch (ts)
            {
                case PlanowanieSalService.TypSali.CWICZENIOWA:
                    nazwa = "ćwiczeniowa";
                    break;
                case PlanowanieSalService.TypSali.LABORATORIUM_CHEMICZNE:
                    nazwa = "laboratorium chemiczne";
                    break;
                case PlanowanieSalService.TypSali.LABORATORIUM_KOMPUTEROWE:
                    nazwa = "laboratorium komputerowe";
                    break;
                case PlanowanieSalService.TypSali.PRACOWNIA_TOKARSKA:
                    nazwa = "pracownia tokarska";
                    break;
                case PlanowanieSalService.TypSali.WYKLADOWA:
                    nazwa = "wykładowa";
                    break;
            }

            return nazwa;
        }

        public static string nazwaJezyk(ZIRPService.Jezyk jezyk)
        {
            string nazwa = "(nieznany)";

            switch (jezyk)
            {
                case ZIRPService.Jezyk.ANGIELSKI:
                    nazwa = "angielski";
                    break;
                case ZIRPService.Jezyk.POLSKI:
                    nazwa = "polski";
                    break;
            }

            return nazwa;
        }

        public static string nazwaTryb(ZIRPService.Tryb tryb)
        {
            string nazwa = "(nieznany)";

            switch (tryb)
            {
                case ZIRPService.Tryb.DZIENNE:
                    nazwa = "dzienne";
                    break;
                case ZIRPService.Tryb.ZAOCZNE:
                    nazwa = "zaoczne";
                    break;
            }

            return nazwa;
        }

        public static string nazwaTypSemestru(ZIRPService.TypSemestru ts)
        {
            string nazwa = "(nieznany)";

            switch (ts)
            {
                case ZIRPService.TypSemestru.LETNI:
                    nazwa = "letni";
                    break;
                case ZIRPService.TypSemestru.ZIMOWY:
                    nazwa = "zimowy";
                    break;
            }

            return nazwa;
        }
    }
}